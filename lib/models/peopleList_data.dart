import 'dart:convert';

import 'package:movie_provider_app/models/people_data.dart';

PeopleList peopleListFromJson(String str) =>
    PeopleList.fromJson(json.decode(str));

class PeopleList {
  PeopleList({
    this.page,
    this.popularPeople,
    this.totalPages,
    this.totalResults,
  });

  int page;
  List<People> popularPeople;
  int totalPages;
  int totalResults;

  factory PeopleList.fromJson(Map<String, dynamic> json) => PeopleList(
        page: json["page"],
        popularPeople:
            List<People>.from(json["results"].map((x) => People.fromJson(x))),
        totalPages: json["total_pages"],
        totalResults: json["total_results"],
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "results": List<dynamic>.from(popularPeople.map((x) => x.toJson())),
        "total_pages": totalPages,
        "total_results": totalResults,
      };
}
