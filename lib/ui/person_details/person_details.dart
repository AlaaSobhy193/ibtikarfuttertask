import 'package:flutter/material.dart';
import 'package:movie_provider_app/ui/person_images/personImages_view.dart';
import 'package:provider/provider.dart';
import 'package:movie_provider_app/ui/person_details/personDetails_provider.dart';

class PersonDetailsView extends StatefulWidget {
  final int id;

  const PersonDetailsView({Key key, this.id}) : super(key: key);
  @override
  _PersonDetailsViewState createState() => _PersonDetailsViewState();
}

class _PersonDetailsViewState extends State<PersonDetailsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[900],
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[900],
        title: Text(
          "Person details",
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
        ),
        leading: InkWell(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_outlined, color: Colors.white)),
      ),
      body: ChangeNotifierProvider<PersonDetailsProvider>(
        create: (BuildContext context) => PersonDetailsProvider(widget.id),
        child: Consumer<PersonDetailsProvider>(
            builder: (context, personDetailsProvider, _) {
          return personDetailsProvider.person != null
              ? SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3),
                            )
                          ],
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(30),
                            bottomLeft: Radius.circular(30),
                          ),
                          image: DecorationImage(
                              image: NetworkImage(
                                "https://image.tmdb.org/t/p/w500/${personDetailsProvider.person.profilePath}",
                              ),
                              fit: BoxFit.cover),
                        ),
                        height: 350,
                        width: double.infinity,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          children: [
                            Text(
                              personDetailsProvider.person.name,
                              style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.amber),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              personDetailsProvider.person.biography,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Icon(Icons.location_on_rounded,
                                    color: Colors.amber),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: Text(
                                    personDetailsProvider.person.placeOfBirth,
                                    maxLines: 2,
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.amber,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 15,
                            ),
                          ],
                        ),
                      ),
                      buildKnownFor(personDetailsProvider),
                      SizedBox(
                        height: 10,
                      ),
                      PersonImagesView(
                        id: personDetailsProvider.person.id,
                      ),
                    ],
                  ),
                )
              : Center(
                  child: CircularProgressIndicator(),
                );
        }),
      ),
    );
  }

  Widget buildKnownFor(PersonDetailsProvider personDetailsProvider) {
    return Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                          children: List<Widget>.generate(
                              personDetailsProvider.person.alsoKnownAs.length,
                              (int index) {
                        return Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8),
                          child: Chip(
                            label: Text(
                              personDetailsProvider.person.alsoKnownAs[index],
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12.0),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50),
                                side: BorderSide(color: Colors.white)),
                            backgroundColor: Colors.blueGrey[900],
                          ),
                        );
                      }).toList()),
                    );
  }
}
