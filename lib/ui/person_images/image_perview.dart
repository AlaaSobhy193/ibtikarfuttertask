import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gallery_saver/gallery_saver.dart';

class ImagePerview extends StatefulWidget {
  final String image;

  const ImagePerview({Key key, this.image}) : super(key: key);
  @override
  _ImagePerviewState createState() => _ImagePerviewState();
}

class _ImagePerviewState extends State<ImagePerview> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[900],
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              height: double.infinity,
              child: Image.network(
                  "https://image.tmdb.org/t/p/w500/${widget.image}"),
            ),
            Positioned(
              right: 5,
              child: PopupMenuButton(
                  icon: Icon(
                    Icons.more_vert,
                    color: Colors.white,
                    size: 30,
                  ),
                  itemBuilder: (BuildContext context) => [
                        PopupMenuItem(
                            value: 1,
                            child: InkWell(
                              onTap: () => saveNetworkImage(),
                              child: Text(
                                "Save",
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ))
                      ]),
            ),
          ],
        ),
      ),
    );
  }

  void saveNetworkImage() async {
    String path = "https://image.tmdb.org/t/p/w500/${widget.image}";
    GallerySaver.saveImage(path).then((bool success) {
      setState(() {
        Fluttertoast.showToast(
            msg: 'Image Saved Success',
            backgroundColor: Colors.amber,
            textColor: Colors.white);
      });
    });
  }
}
