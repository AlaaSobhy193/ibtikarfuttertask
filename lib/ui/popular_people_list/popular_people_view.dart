import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:movie_provider_app/models/people_data.dart';
import 'package:movie_provider_app/ui/popular_people_list/popular_people_list_provider.dart';
import 'package:movie_provider_app/widgets/people_item.dart';
import 'package:provider/provider.dart';

class PopularPeopleView extends StatefulWidget {
  const PopularPeopleView({Key key}) : super(key: key);
  @override
  _PopularPeopleViewState createState() => _PopularPeopleViewState();
}

class _PopularPeopleViewState extends State<PopularPeopleView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blueGrey[900],
        appBar: AppBar(
          backgroundColor: Colors.blueGrey[900],
          title: Text(
            'Popular People',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        body: ChangeNotifierProvider<PopularPeopleProvider>(
          create: (BuildContext context) => PopularPeopleProvider(),
          child: Consumer<PopularPeopleProvider>(
              builder: (context, popularPeopleProvider, _) {
            return popularPeopleProvider.people != null
                ? PagedListView<int, People>(
                    pagingController: popularPeopleProvider.pagingController,
                    builderDelegate: PagedChildBuilderDelegate<People>(
                      itemBuilder: (context, item, index) => PopularPeopleItem(
                        people: item,
                      ),
                    ),
                  )
                : Center(child: CircularProgressIndicator());
          }),
        ));
  }
}
