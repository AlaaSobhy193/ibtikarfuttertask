import 'package:flutter/foundation.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:movie_provider_app/models/peopleList_data.dart';
import 'package:movie_provider_app/models/people_data.dart';
import 'package:movie_provider_app/repositories/people_repository.dart';

class PopularPeopleProvider extends ChangeNotifier {
  PeopleList people;
  int page;
  static const _pageSize = 1;

  PagingController<int, People> pagingController =
      PagingController(firstPageKey: 1);

  PeopleRepository _peopleRepository = PeopleRepository();

  PopularPeopleProvider() {
    getPopularPeople();
    pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
  }
  void getPopularPeople() {
    _peopleRepository.fetchPopularPeople(page).then((result) {
      people = result;
      notifyListeners();
    });
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newPeople = await _peopleRepository.fetchPopularPeople(pageKey);
      final isLastPage = newPeople.popularPeople.length < _pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(newPeople.popularPeople);
      } else {
        final nextPageKey = pageKey + newPeople.popularPeople.length;
        pagingController.appendPage(newPeople.popularPeople, nextPageKey);
      }
    } catch (error) {
      pagingController.error = error;
    }
  }
}
